<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new Laravel application instance
| which serves as the "glue" for all the components of Laravel, and is
| the IoC container for the system binding all of the various parts.
|
*/

function tbpath(string $relative = '') :string
{
    $path = getenv('TAILORBIRD_PATH') ?: getenv('HOME').'/.config/tailorbird/';
    return rtrim($path, '/') . '/' . $relative;
}

$app = new LaravelZero\Framework\Application(
    dirname(__DIR__)
);

/*
|--------------------------------------------------------------------------
| Bind Important Interfaces
|--------------------------------------------------------------------------
|
| Next, we need to bind some important interfaces into the container so
| we will be able to resolve them when needed. The kernels serve the
| incoming requests to this application from both the web and CLI.
|
*/

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    LaravelZero\Framework\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    Illuminate\Foundation\Exceptions\Handler::class
);

$fs   = new Illuminate\Filesystem\Filesystem;
$path = tbpath();

if (!$fs->exists($path)) {
    $fs->makeDirectory($path);
}

if (!$fs->exists($path)) {
    $str = 'Could not create tailorbird directory, ' . $path;
    echo "\033[31m$str \033[0m" . PHP_EOL;
    $basedir = dirname($path);
    echo "Does the directory, $basedir, exist?" . PHP_EOL;
    die;
}

$file = "{$path}tailorbird.env";
if (!$fs->exists($file)) {
    $fs->put($file, new App\Examples\EnvExample());
}
Dotenv\Dotenv::createMutable($path, 'tailorbird.env')->load();

/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/

return $app;
