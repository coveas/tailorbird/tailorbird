<?php

use App\Actions\ImportSQLFileToDatabaseAction;
use App\Actions\ConvertsSQLFileFromMariaDBToMysqlAction;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

$sql = <<<SQL
CREATE TABLE `test_table` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `date_created` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_trash` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=Aria AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 PAGE_CHECKSUM=1;
SQL;
$sql = gzencode($sql);
if ($sql) {
    beforeEach(fn () => Storage::put('tests/test.sql.gz', $sql));
    afterEach(fn() => Storage::delete('tests/test.sql.gz'));
}


$skipImportDatabase = function () {
    $username = 'root';
    $password = 'root';
    $port     = '3308';
    $host     = '127.0.0.1';
    $command = [
        'mysql',
        '-u' . $username,
        '-p' . $password,
        '-P' . $port,
        '-h' . $host,
        '-eSHOW DATABASES'
    ];
    $process = new Process($command);
    $process->run();
    if (! $process->isSuccessful()) {
        return true;
    }
    return false;
};

it(
    'Importing from MariaDB to MySQL creates PAGE_CHECKSUM error',
    function () {
        putenv("MYSQL_BINARY=");
        putenv("MYSQL_PORT=3308");
        putenv("MYSQL_PASSWORD=root");
        putenv("MYSQL_USER=root");
        $path = Storage::path('tests/test.sql.gz');
        (new ImportSQLFileToDatabaseAction($path))('test');
    }
)->throws(
    ProcessFailedException::class, "'PAGE_CHECKSUM=1' at line"
)->skip($skipImportDatabase, 'Could not connect to MySQL8 at port 3308');

it(
    'Removes PAGE_CHECKSUM from SQL file',
    function () {
        $path    = Storage::path('tests/test.sql.gz');
        $newPath = Storage::path('tests/new.sql.gz');
        (new ConvertsSQLFileFromMariaDBToMysqlAction($path))($newPath);
        $gzipped = Storage::get('tests/new.sql.gz');
        $content = (string) gzdecode($gzipped);
        expect($content)->not->toContain('PAGE_CHECKSUM');
    }
);

it(
    'Imports after converting SQL file from MariaDB to Mysql format',
    function () {
        putenv("MYSQL_BINARY=");
        putenv("MYSQL_PORT=3308");
        putenv("MYSQL_PASSWORD=root");
        putenv("MYSQL_USER=root");
        $path    = Storage::path('tests/test.sql.gz');
        $newPath = Storage::path('tests/new.sql.gz');
        (new ConvertsSQLFileFromMariaDBToMysqlAction($path))($newPath);
        $result = (new ImportSQLFileToDatabaseAction($newPath))('test');
        expect($result)->toBe('');
    }
)->skip($skipImportDatabase, 'Could not connect to MySQL8 at port 3308');
