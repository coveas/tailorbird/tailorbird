<?php

namespace App\Factories;

use App\Actions\GetApiTokenAction;
use App\Models\Model;
use Exception;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Http;
use App\Models\Project;
use LaravelZero\Framework\Commands\Command;

class ProjectFactory
{
    public function __construct(protected Command $command)
    {
    }

    /**
     * @throws RequestException
     * @throws Exception
     */
    public function fromSlug(string $slug): Model
    {
        $newToken = false;
        $base_url = tbconfig('api_provider');

        /** @var GetApiTokenAction $action */
        $action = app(GetApiTokenAction::class);
        $token = $action($this->command);

        if (empty($token)) {
            $this->command->warn("No token provided. Exiting.");
        }

        $response = Http::withToken($token)->get(
            $base_url.'api/projects'
        )->throw();


        if (empty($response->json())) {
            throw new Exception("Could not get projects. Is your API_KEY correct?", 1);
        }
        if ($newToken && $this->command->confirm("Would you like to save the API token?", true)) {
            $this->saveToken($token);
        }
        $projects   = collect($response->json('data'));
        $candidates = $projects->filter(
            function ($project) use ($slug) {
                if (in_array($slug, $project['aliases'])) {
                    return true;
                }
                return $project['slug'] === $slug;
            }
        );
        if ($candidates->isEmpty()) {
            throw new Exception("Could not find project, $slug", 1);
        }
        /** @var array $candidate */
        $candidate = $candidates->first();
        if ($candidates->count() > 1) {
            // Pick one
        }

        $project = Project::make(
            [
                'slug' => $candidate['slug'],
            ]
        );

        $project->name               = $candidate['name'];
        $project->git_repo           = $candidate['git_repo'];
        $project->development_branch = $candidate['development_branch'];
        $project->instances          = collect();
        foreach ($candidate['instances'] as $instanceData) {
            $instance = InstanceFactory::fromArray($instanceData);

            $instance->project = $project;
            $project->instances->add($instance);
        }

        return $project;
    }

    public function saveToken(string $token): void
    {
        $this->command->info($token);

        $path = tbpath();

        $token   = str_replace('\'', '\\\'', $token);
        $fs      = new Filesystem();
        $content = $fs->get("{$path}tailorbird.env");
        $exp     = '/(^\s*API_KEY=)\s*$/m';
        if (preg_match($exp, $content)) {
            $content = preg_replace($exp, "API_KEY={$token}", $content);
        } else {
            $content = trim($content).PHP_EOL."API_KEY={$token}".PHP_EOL;
        }

        $fs->put("{$path}tailorbird.env", "$content");
        $this->command->info("Saved API token, $token, in the tailorbird configuration file: {$path}tailorbird.env");
    }
}
