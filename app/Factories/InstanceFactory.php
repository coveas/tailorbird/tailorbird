<?php

namespace App\Factories;

use App\Models\Instance;

class InstanceFactory
{
    public static function fromArray(array $data): Instance
    {
        return Instance::make(
            [
                'slug'      => $data['slug'] ?? '',
                'user'      => $data['username'] ?? '',
                'url'       => $data['url'] ?? '',
                'ssh_port'  => $data['ssh_port'] ?? null,
                'sftp_port' => $data['sftp_port'] ?? null,
                'db'        => $data['db'] ?? '',
                'host'      => $data['host'] ?? '',
                'db_host'   => $data['db_host'] ?? '',
                'path'      => $data['path'] ?? '',
            ]
        );
    }
}
