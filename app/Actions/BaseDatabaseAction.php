<?php

namespace App\Actions;

use App\Commands\Command;
use App\Models\Instance;
use Exception;
use JetBrains\PhpStorm\ArrayShape;

class BaseDatabaseAction
{
    #[ArrayShape(
        [
            'name'     => 'string',
            'user'     => 'string',
            'password' => 'string',
        ]
    )]
    public array    $params;
    public Instance $instance;
    public ?Command $command;

    /**
     * @throws Exception
     */
    public function initParams(): void
    {
        $path          = $this->instance->getPath();
        $remoteCommand = "if [ -f \"{$path}/.env\" ]; then\ncat {$path}/.env | grep DB\nfi";
        if ($this->command && ($this->command->option('dry') || $this->command->option('verbose'))) {
            $this->command->info('Running the following remote command');
            $this->command->line($remoteCommand);
        }
        $this->instance->ssh()->exec(
            $remoteCommand,
            // @phpstan-ignore-next-line
            function ($line) {
                $this->params = $this->parseDb($line);
            }
        );
        if (empty($this->params)) {
            throw new Exception("Unable to scan .env file on target");
        }
    }

    #[ArrayShape(
        [
            'user'     => "string",
            'password' => "string",
            'name'     => "string"
        ]
    )]
    protected function parseDb(string $content): array
    {
        $params = [
            'user'     => '',
            'password' => '',
            'name'     => '',
        ];
        $lines  = explode("\n", $content);
        foreach ($lines as $line) {
            if (preg_match('/^DB_USER=[\'"]?(.*?)[\'"]?$/', $line, $matches)) {
                $params['user'] = $matches[1];
            }
            if (preg_match('/^DB_PASSWORD=[\'"]?(.*?)[\'"]?$/', $line, $matches)) {
                $params['password'] = $matches[1];
            }
            if (preg_match('/^DB_NAME=[\'"]?(.*?)[\'"]?$/', $line, $matches)) {
                $params['name'] = $matches[1];
            }
            if (preg_match('/^DB_DATABASE=[\'"]?(.*?)[\'"]?$/', $line, $matches)) {
                $params['name'] = $matches[1];
            }
            if (preg_match('/^DB_USERNAME=[\'"]?(.*?)[\'"]?$/', $line, $matches)) {
                $params['user'] = $matches[1];
            }
        }
        return $params;
    }
}
