<?php

namespace App\Actions;

use ErrorException;
use App\Models\Instance;
use Exception;
use Illuminate\Support\Facades\Storage;
use JetBrains\PhpStorm\ArrayShape;

class UploadDatabaseAction extends BaseDatabaseAction
{
    public function __construct(public Instance $instance, public string $filename)
    {
    }

    /**
     * @throws Exception
     */
    public function handle(): void
    {
        $ssh  = $this->instance->ssh();
        $sftp  = $this->instance->sftp();
        $this->initParams();
        $tmpfile = '/tmp/' . uniqid();

        try {
            $sftp->put(
                Storage::path($this->filename),
                $tmpfile
            );
        } catch (ErrorException $e) {
            sleep(1);
            $sftp->put(
                Storage::path($this->filename),
                $tmpfile
            );
        }
        [$name,$user,$password] = $this->params;

        $command = "cat $tmpfile | gunzip | mysql {$name} -u{$user} -p{$password}";
        $ssh->run($command); // @phpstan-ignore-line
    }
}
