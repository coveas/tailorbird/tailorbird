<?php


namespace App\Actions;


use Exception;

class GetMysqlBinaryAction
{
    private string $orignalPath = '';

    /**
     * @throws Exception
     */
    public function __invoke():string
    {
        $path = '' . env('MYSQL_BINARY');
        if (! $path) {
            if (is_dir('/Users/Shared/DBngin/mysql')) {
                $path = '/Users/Shared/DBngin/mysql/{latest}/bin/mysql';
            } else {
                $path = 'mysql';
            }
        }
        $this->orignalPath = $path;
        $path = $this->parse($path);
        return $this->testPath($path);
    }

    /**
     * @throws Exception
     */
    private function testPath(string $path):string
    {
        if (! preg_match('/^\//', $path)) {
            return $path;
        }
        if (is_file($path)) {
            return $path;
        }
        if (is_file("$path/mysql")) {
            return "$path/mysql";
        }
        if (is_file("$path/bin/mysql")) {
            return "$path/bin/mysql";
        }
        throw new Exception("MYSQL binary was not found at {$this->orignalPath}.");
    }

    private function parse(string $path):string
    {
        $parts = explode('{latest}', $path);
        $end = array_pop($parts);

        $newParts = [];
        foreach ($parts as $part) {
            $files = glob("$part*");
            if (! is_array($files)) {
                continue;
            }
            natsort($files);
            $newParts[] = array_pop($files);
        }
        return join('', $newParts) . $end;
    }
}
