<?php
namespace App\Actions;

use App\Models\Project;
use ErrorException;
use App\Models\Instance;
use Illuminate\Support\Facades\Storage;

class GitCloneRepoAction
{
    public function __construct(
        public Project $project,
        public string $target
    )
    {
    }

    public function handle(): void
    {
        $git_repo = preg_replace(
            '/^https:\/\/([^\/]*)\//',
            'git@$1:',
            "{$this->project->git_repo}"
        );

        $command = sprintf(
            'git clone %s %s',
            escapeshellarg("$git_repo"),
            escapeshellarg($this->target)
        );

        if ($this->project->development_branch) {
            $command .= sprintf(
                ' && cd %s && git checkout %s && cd ..',
                escapeshellarg($this->target),
                escapeshellarg("{$this->project->development_branch}")
            );
        }

        passthru($command);
    }
}
