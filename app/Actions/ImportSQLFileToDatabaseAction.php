<?php

namespace App\Actions;

use App\Exceptions\FileTooSmallException;
use App\Models\Project;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class ImportSQLFileToDatabaseAction
{
    public ?Command $command = null;

    public function __construct(public string $filename)
    {
    }

    /**
     * @throws FileTooSmallException
     */
    public function __invoke(string $target):string
    {
        $target   = preg_replace('/[\W]/', '', $target);
        $mysqlBin = app(GetMysqlBinaryAction::class)();
        $username = ''. env('MYSQL_USERNAME', 'root');
        $password = ''. env('MYSQL_PASSWORD', '');
        $port     = ''. env('MYSQL_PORT', '3306');
        $host     = ''. env('MYSQL_HOST', '127.0.0.1');

        $fs = new Filesystem();

        if ($fs->size($this->filename) <= 50) {
            throw new FileTooSmallException($this->filename);
        }

        $auth = '';
        if (! empty($username)) {
            $auth .= "-u{$username}";
        }
        if (! empty($password)) {
            $auth .= " -p{$password}";
        }
        $args     = sprintf(
                '(echo "DROP DATABASE IF EXISTS %s; CREATE DATABASE %1$s; USE %1$s;"; cat %s  | gunzip)',
                $target,
                $this->filename,
            )."| {$mysqlBin} {$auth} -P{$port} -h{$host}";
        $command  = $this->command;

        if ($command) {
            if ($command->option('dry') || $command->option('verbose')) {
                $command->info(($command->option('dry')  ? 'Would run' : 'Running') . ' the following command');
                $command->line($args);
            }
            if ($command->option('dry')) {
                return '';
            }
        }

        $process = Process::fromShellCommandline($args);
        $timeout = floatval(env('COMMAND_TIMEOUT', 1800));
        $process->setTimeout($timeout);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return $process->getOutput();
    }
}
