<?php


namespace App\Actions;


class ConvertsSQLFileFromMariaDBToMysqlAction
{

    /**
     * RemovesPageChecksumFromSQLAction constructor.
     * @param  string  $path
     */
    public function __construct(private string $path)
    {
    }


    public function __invoke(string $target):void
    {
        $reader = gzopen($this->path, 'r');
        $writer = gzopen($target, 'w');

        if (! $reader || ! $writer) {
            return;
        }

        while (!gzeof($reader)) {
           $buffer = gzgets($reader);
           if (! is_string($buffer)) {
               continue;
           }
           $buffer = str_replace(' PAGE_CHECKSUM=1', '', $buffer);
           $buffer = str_replace('ENGINE=Aria ', 'ENGINE=InnoDB ', $buffer);
           gzwrite($writer, $buffer);
        }

        gzclose($reader);
        gzclose($writer);
    }
}
