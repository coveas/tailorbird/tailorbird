<?php

namespace App\Actions;

use App\Commands\Command;
use App\Exceptions\SshCommandException;
use ErrorException;
use App\Models\Instance;
use Exception;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;

class DownloadDatabaseAction extends BaseDatabaseAction
{
    public string $server_tmp;

    public function __construct(public Instance $instance, public ?Command $command)
    {
    }

    /**
     * @throws Exception
     */
    public function __invoke(): string
    {
        $fs       = new Filesystem();
        $filename = $this->getFileName();
        if (
            $fs->exists($filename)
            && $fs->size($filename) >= 50
            && $this->command?->confirm('Re-import previously downloaded database?', true)
        ) {
            return $filename;
        }
        $commandFile = "tailorbird-{$this->instance->slug}-db-download.sh";
        if (file_exists($commandFile)) {
            $this->command?->warn('Using '.$commandFile);
            $command  = file_get_contents($commandFile);
            $filename = $this->getFileName();
            $config   = $this->instance->getConfig();
            $command  = strtr(
                "$command",
                [
                    '{username}' => $config['username'],
                    '{host}'     => $this->instance->getHost(),
                    '{filename}' => Storage::path($filename),
                ]
            );
            shell_exec($command);
            return $filename;
        }

        $ssh  = $this->instance->ssh();
        $sftp = $this->instance->sftp();

        $this->initParams();
        extract($this->params);
        if (empty($name)) {
            $name = $this->instance->db;
        }
        $tmpfile          = '/tmp/db-'.uniqid().'.sql.gz';
        $this->server_tmp = $tmpfile;
        $auth             = '';
        $host             = '';

        if (!empty($user)) {
            $auth .= "-u{$user}";
        }
        if (!empty($password)) {
            $password = str_replace('$', '\$', $password);
            $auth .= " -p\"{$password}\"";
        }
        if (!empty("{$this->instance->db_host}")) {
            $parts = explode(':', $this->instance->db_host);
            $host = "-h\"{$parts[0]}\"";
            if (count($parts) > 1) {
                $host = "-h\"{$parts[0]}\" -P {$parts[1]}";
            }
        }
        $command = "mysqldump {$name} {$host} {$auth} --no-tablespaces | gzip > $tmpfile";

        if ($this->command && ($this->command->option('dry') || $this->command->option('verbose'))) {
            $this->command->info(($this->command->option('dry') ? 'Would run' : 'Running').' the following remote command:');
            $this->command->line($command);
        }
        if (!$this->command || !$this->command->option('dry')) {
            $error = trim($ssh->exec($command));
            $this->processError($error);
        }

        if (!$fs->exists($this->getDir())) {
            $fs->makeDirectory($this->getDir(), 0755, true);
        }
        if ($this->command && $this->command->option('dry')) {
            $this->command->info("Would download $tmpfile to ".$this->getFileName());
            $this->command->info("Would delete $tmpfile");
        } else {
            try {
                $sftp->get(
                    $tmpfile,
                    $this->getFileName()
                );
            } catch (ErrorException $e) {
                sleep(1);
                $sftp = $this->instance->sftp(true);
                $sftp->get(
                    $tmpfile,
                    $this->getFileName()
                );
            }
            $sftp->delete($tmpfile);
        }
        return $this->getFileName();
    }

    public function getFileName(): string
    {
        return $this->getDir()."/{$this->instance->project->slug}_{$this->instance->slug}.sql.gz";
    }

    public function getDir(): string
    {
        static $dir = '';
        if (empty($dir)) {
            $dir = tbpath('/cache/'.date('Y/m/d'));
        }
        return $dir;
    }

    /**
     * @throws SshCommandException
     */
    private function processError(string $error):void
    {
        if (!$error) {
            return;
        }
        $lines = explode("\n", $error);
        foreach ($lines as $line) {
            if (preg_match('/Warning.*Using a password on the command line interface can be insecure/', $line)) {
                continue;
            }
            throw new SshCommandException($error);
        }
    }
}
