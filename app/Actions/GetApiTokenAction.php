<?php

namespace App\Actions;

use LaravelZero\Framework\Commands\Command;

class GetApiTokenAction
{
    public bool $new = false;

    public function __invoke(Command $command): string
    {
        $token = tbconfig('api_key');
        if (empty($token)) {
            /** @var string $token */
            $command->info("Please enter your API token.");
            $command->info("If you need a new one then you can create one here: https://tailorbird.dev/user/api-tokens");
            $token     = $command->ask("Token");
            $this->new = true;
        }
        return $token;
    }
}
