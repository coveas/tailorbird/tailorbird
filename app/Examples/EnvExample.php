<?php
namespace App\Examples;

class EnvExample
{
    function __toString()
    {
        $home = getenv("HOME");
        return <<<ENV
        #; API Provider
        API_PROVIDER="https://tailorbird.dev/"

        #; API Key
        #; The api key is used to connect to the tailorbird provider and download project information and configurations.
        API_KEY=

        #; SSH
        #; SSH Key and passphrase used to connect to servers and download content
        #SSH_KEY="{$home}/.ssh/id_rsa"
        #SSH_KEYPHRASE=

        DEFAULT_USER=forge

        #; Composer configuration
        # COMPOSER_BIN=composer
        # COMPOSER_TIMEOUT=1800

        #; MYSQL Configuration
        # MYSQL_BINARY=/Users/Shared/DBngin/mysql/{latest}
        # MYSQL_USERNAME=root
        # MYSQL_PASSWORD=
        # MYSQL_PORT=3306
        # MYSQL_HOST=127.0.0.1
        ENV;
    }
}
