<?php

namespace App\Models;

use Exception;

/**
 * @property string|null $name
 * @property string|null $git_repo
 * @property string|null $development_branch
 * @property string|null $slug
 */
abstract class Model implements ModelInterface
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected array $fillable   = [
    ];
    protected array $attributes = [];

    final public function __construct()
    {
    }

    public static function make(array $attributes): static
    {
        $model             = new static();
        $model->attributes = $attributes;
        return $model;
    }

    /**
     * @throws Exception
     */
    public function __get(string $name): ?string
    {
        if (!isset($this->attributes[$name]) && !in_array($name, $this->fillable)) {
            throw new Exception('Undefined attribute: '.static::class."::{$name}");
        }
        return $this->attributes[$name] ?? null;
    }
}
