<?php

namespace App\Models;

use Exception;
use Illuminate\Support\Collection;

class Project extends Model
{
    /**
     * @var Collection<Instance>
     */
    public Collection $instances;

    protected array $fillable = [
        'name',
        'slug',
        'git_repo',
        'development_branch',
    ];

    /**
     * @throws Exception
     */
    public function getInstance(string $name): Instance
    {
        if (! isset($this->instances)) {
            $this->instances = collect();
        }
        $instances = $this->instances->filter(
            function ($instance) use ($name) {
                return $instance->slug == $name;
            }
        );
        if ($instances->isEmpty()) {
            throw new Exception("Could not find any instances by name, $name", 1);
        }
        return $instances->first();
    }
}
