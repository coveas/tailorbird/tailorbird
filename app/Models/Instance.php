<?php

namespace App\Models;

use InvalidArgumentException;
use JetBrains\PhpStorm\ArrayShape;
use phpseclib3\Crypt\PublicKeyLoader;
use phpseclib3\Net\SFTP;
use phpseclib3\Net\SSH2;
use phpseclib3\System\SSH\Agent;

/**
 * @property string|null $user
 * @property string|null $host
 * @property string|null $path
 * @property string|null $url
 * @property int|null    $ssh_port
 * @property int|null    $sftp_port
 * @property string|null $db
 * @property string|null $db_host
 */
class Instance extends Model
{
    protected SSH2  $sshConnection;
    protected SFTP  $sftpConnection;
    protected array $fillable = [
        'slug',
        'user',
        'path',
        'ssh_port',
        'sftp_port',
        'url',
        'db',
        'db_host',
        'host',
    ];

    public Project $project;

    public function getUrl(): ?string
    {
        if (preg_match('/^https?:\/\//', "$this->url")) {
            return $this->url;
        }
        if (preg_match('/^\/\/?/', "$this->url")) {
            return $this->url;
        }
        return "https://$this->url";
    }

    public function getDomain(): ?string
    {
        $url = $this->getUrl();
        if (! $url) {
            return null;
        }
        $result = parse_url($url, PHP_URL_HOST);
        return is_string($result) ? $result : null;
    }

    public function getUser(): string
    {
        if (!$this->user) {
            return tbconfig('default_user');
        }
        return strtr(
            $this->user,
            [
                '{username}' => trim(`whoami`),
            ]
        );
    }

    public function getHost(): ?string
    {
        return $this->host ?: $this->getDomain();
    }

    public function getPath(): string|null
    {
        $path = $this->path;
        if (empty($path)) {
            return '/home/'.$this->getUser().'/'.$this->getDomain();
        }
        return str_replace('//', '/', $path);
    }


    #[ArrayShape([
        'host'     => "mixed",
        'port'     => "int|string",
        'username' => "string",
        'agent'    => "string",
        'gateway'  => "null",
        'timeout'  => "int"
    ])] public function getConfig(string $type = 'ssh'): array
    {
        $config = [
            'host'     => $this->getHost(),
            'port'     => ($this->ssh_port ?? 22),
            'username' => $this->getUser(),
            'agent'    => '',
            'gateway'  => null,
            'timeout'  => 10,
        ];
        if ('sftp' === $type) {
            $config['port'] = ($this->sftp_port ?? 22);
        }
        return $config;
    }

    /**
     * Format the appropriate authentication array payload.
     * Copied from Collective\Remote\RemoteManager@getAuth
     *
     * @param array $config
     *
     * @return array
     * @throws InvalidArgumentException
     *
     */
    protected function getAuth(array $config): array
    {
        if (isset($config['agent']) && $config['agent'] === true) {
            return ['agent' => true];
        } elseif (isset($config['key']) && trim($config['key']) != '') {
            return ['key' => $config['key'], 'keyphrase' => $config['keyphrase']];
        } elseif (isset($config['keytext']) && trim($config['keytext']) != '') {
            return ['keytext' => $config['keytext']];
        } elseif (isset($config['password'])) {
            return ['password' => $config['password']];
        }
        throw new InvalidArgumentException('Password / key is required.');
    }

    public function ssh(bool $force = false): SSH2
    {
        $config = $this->getConfig();
        if (empty($this->sshConnection) || $force) {
            $keyFile = tbconfig('ssh_key');
            $agent = new Agent;
            $key     = PublicKeyLoader::load(
                file_get_contents($keyFile) ?: '',
                tbconfig('ssh_keyphrase')
            );

            $ssh     = new SSH2($config['host'], $config['port']);
            if (!$ssh->login($config['username'], $agent, $key)) {
                throw new \Exception("Could not log in to server, {$config['username']}@{$config['host']} using ssh-agent");
            }
            $this->sshConnection = $ssh;
        }
        return $this->sshConnection;
    }

    public function sftp(bool $force = false): SFTP
    {
        $config = $this->getConfig();
        if (empty($this->sftpConnection) || $force) {
            $keyFile = tbconfig('ssh_key');
            $key     = PublicKeyLoader::load(
                file_get_contents($keyFile) ?: '',
                tbconfig('ssh_keyphrase')
            );
            $agent = new Agent;
            $sftp    = new SFTP($config['host'], $config['port']);
            if (!$sftp->login($config['username'], $agent, $key)) {
                throw new \Exception("Could not log in to server, {$config['host']} using ssh-agent");
            }
            $this->sftpConnection = $sftp;
        }
        return $this->sftpConnection;
    }
}
