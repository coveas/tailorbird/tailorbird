<?php

namespace App\Models;

interface ModelInterface
{
    public function __get(string $name): ?string;
}
