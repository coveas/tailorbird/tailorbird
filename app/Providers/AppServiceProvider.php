<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Phar;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        config(
            [
                'logging.channels.single.path' => Phar::running()
                    ? tbpath('logs/tailorbird.log')
                    : storage_path('logs/tailorbird.log')
            ]
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
