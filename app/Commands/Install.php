<?php

namespace App\Commands;

use App\Actions\GitCloneRepoAction;
use App\Models\Project;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

class Install extends ProjectCommand
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'install
                            {slug? : (optional) The short name for the project. By default it will use the current directory as the slug.}
                            {target? : (optional) Target directory to install to. By default it will use the slug of the project as the target directory.}
                            {--no-composer : Don\'t ask to install composer packages}
                            {--no-npm : Don\'t ask to install npm packages}
                            {--no-yarn : Don\'t ask to install yarn packages}
                            {--no-database : Don\'t ask to download and import the database}
                            {--D|dry : Do a dry run (won\'t make any changes)}';

    /**
     * The description of the command.
     *
     * Search for a project using its slug and then install the project if is found.
     * First it will clone the git repository. After cloning it will run install for any
     * package managers that are present. Once packages are installed it will attempt to
     * download and import the database if there is a "live" instance of the project.
     * Finally it will ask to initialise any configuration files
     *
     * @var string
     */
    protected $description = 'Search for and install project by its slug';


    public function __construct(protected Filesystem $fs)
    {
        parent::__construct();
    }

    protected function isInTarget(string $target): bool
    {
        $cwd     = getcwd();
        $parts   = explode('/', "$cwd");
        $dirname = array_pop($parts);
        // Check if the directory exists.
        return ($target === $dirname) && !$this->fs->isDirectory($target);

    }

    protected function targetIsEmpty(string $target): bool
    {
        if (!$this->fs->exists($target) || !$this->fs->isDirectory($target)) {
            return true;
        }
        $files = $this->fs->files($target);
        return empty($files);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {

        [$project, $target] = $this->getProjectArguments(true);


        $inTarget = $this->isInTarget($target);
        if (!$inTarget && ! $this->cloneAndChangeWorkingDirectory($project, $target)) {
            // Unable to clone.
            return 1;
        }

        $files = scandir('.');
        if (! is_array($files)) {
            $this->error('Could not scan current directory');
            return Command::FAILURE;
        }
        if (
            in_array('composer.json', $files) &&
            $this->confirm('Run composer install', true)
        ) {
            $this->process(['composer', 'install']);
        }

        if (
            in_array('package-lock.json', $files) &&
            $this->confirm('Run npm install?', true)
        ) {
            $this->process(['npm', 'install']);
        }

        if (
            in_array('yarn.lock', $files) &&
            $this->confirm('Run yarn install?', true)
        ) {
            $this->process(['yarn', 'install']);
        }

        $content = '';

        if (
            in_array('.env.example', $files) &&
            ! $this->fs->exists('.env') &&
            $this->confirm('Copy .env.example to .env?', true)
        ) {
            try {
                $content = $this->fs->get('.env.example');
            } catch (FileNotFoundException $e) {
            }
            $content = preg_replace('/^(DB_NAME=).*$/m', "\$1{$project->slug}", $content);
            if ($this->option('dry')) {
                $this->info('Would copy .env.example to .env');
            } else {
                $this->fs->put('.env', "$content");
            }

        }

        if (empty($content)) {
            try {
                $content = $this->fs->get('.env');
            } catch (FileNotFoundException $e) {
            }
        }

        if (! empty($content)) {
            if ($this->shouldGenerateSalt($content)) {
                $this->process(['php', 'generate-salt.php']);
            }
            if ($this->shouldGenerateAppKey($content)) {
                $this->process(['php', 'artisan', 'key:generate']);
            }
        }

        if (
            $this->shouldDownloadDatabase("$content")
        ) {
            $this->call('download:database', [
                'slug' => $project->slug,
                '--no-interaction' => $this->option('no-interaction'),
                '--dry' => $this->option('dry'),
            ]);
        }
        return 0;
    }

    private function shouldGenerateAppKey(string $content):bool
    {
        if (! preg_match('/^APP_KEY=$/m', $content)) {
            return false;
        }
        return $this->confirm('Generate APP_KEY?', true);
    }

    private function shouldDownloadDatabase(string $content):bool
    {
        if (! preg_match('/^DB_/m', $content)) {
            return false;
        }
        return $this->confirm('Download database?', true);
    }

    private function shouldGenerateSalt(string $content):bool
    {
        if (! preg_match('/^SECURE_AUTH_SALT=$/m', $content)) {
            return false;
        }
        return $this->confirm('Generate salt?', true);
    }

    private function cloneAndChangeWorkingDirectory(Project $project, string $target):bool
    {
        if ($this->targetIsEmpty($target)) {
            $action = new GitCloneRepoAction($project, $target);
            $action->handle();

            if (! $this->fs->exists($target)) {
                $this->error('Failed to clone repo');
                return false;
            }
        }
        chdir($target);
        return true;
    }

}
