<?php

namespace App\Commands;

use App\Actions\GetApiTokenAction;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use LaravelZero\Framework\Commands\Command;
use Symfony\Component\Console\Command\Command as SymfonyCommand;

class Init extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'init
                           {name? : The name of the project (optional)}
                           {team? : The team name who would have access to (optional)}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Creates a new project on tailorbird.dev';

    /**
     * Base URL of provider.
     *
     * @var string|null
     */
    protected ?string $baseUrl = null;

    public function __construct(protected Filesystem $fs)
    {
        parent::__construct();

        $this->baseUrl = tbconfig('api_provider');
    }

    /**
     * Execute the console command.
     *
     * @throws RequestException
     */
    public function handle(): int
    {
        $team = $this->argument('team');

        /** @var GetApiTokenAction $action */
        $action = app(GetApiTokenAction::class);
        $token = $action($this);

        if (!$team) {
            try {
                $teams = Http::acceptJson()
                    ->withToken($token)
                    ->post("$this->baseUrl/api/teams")
                    ->throw()
                    ->json('teams');
            } catch (RequestException $exception) {
                $this->revokeToken($token);
                $this->error($exception->response->reason());

                return SymfonyCommand::FAILURE;
            }

            $team = $this->choice('Please, select a team:', $teams);
        }

        $projectSlug = $this->ask('Please, provide the project slug (optional)', $this->getProjectSlug());

        try {
            Http::acceptJson()
                ->withToken($token)
                ->post(
                    "$this->baseUrl/api/projects",
                    [
                        'name'     => $this->getProjectName(),
                        'slug'     => $projectSlug,
                        'git_repo' => $gitRepoUrl ?? null,
                        'team'     => $team,
                    ]
                )
                ->throw()
                ->json('project');

            $this->revokeToken($token);
            $this->info('Project created');

            return SymfonyCommand::SUCCESS;
        } catch (RequestException $exception) {
            $this->revokeToken($token);
            $this->error($exception->response->reason());

            return SymfonyCommand::FAILURE;
        }
    }

    protected function gitDirectoryExists(): bool
    {
        return $this->fs->exists($this->getProjectPath().'/.git');
    }

    protected function getProjectPath(): string
    {
        return $this->fs->basename(getcwd()) === $this->getProjectName()
            ? '.'
            : $this->getProjectName();
    }

    protected function getProjectName(): string
    {
        return $this->argument('name') ?? $this->fs->basename(getcwd());
    }

    protected function getProjectSlug(): string
    {
        return Str::before($this->getProjectName(), '.');
    }

    /**
     * @throws RequestException
     */
    protected function revokeToken(string $token): void
    {
        Http::acceptJson()
            ->withToken($token)
            ->post("$this->baseUrl/api/auth/tokens/logout")
            ->throw();
    }
}
