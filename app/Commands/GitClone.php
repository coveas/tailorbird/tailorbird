<?php

namespace App\Commands;

use App\Actions\GitCloneRepoAction;

class GitClone extends ProjectCommand
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'clone {slug} {target?}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Use git to clone a project';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        [$project, $target] = $this->getProjectArguments(true);

        $action = new GitCloneRepoAction($project, $target);
        $action->handle();
    }
}
