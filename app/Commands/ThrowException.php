<?php

namespace App\Commands;

use Exception;

class ThrowException extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'throw-exception';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Throws an exception';

    protected $hidden = true;

    /**
     * Execute the console command.
     *
     * @return int./
     * @throws Exception
     */
    public function handle(): int
    {
        throw new Exception();
    }

}
