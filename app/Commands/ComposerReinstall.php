<?php

namespace App\Commands;

use Illuminate\Support\Collection;

class ComposerReinstall extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'cre {--D|dry}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Reinstall composer packages';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!file_exists('composer.json')) {
            $this->error('Missing composer.json.');
            return;
        }

        $content = file_get_contents('composer.json');

        if (!$content) {
            $this->error('The composer.json is empty.');
            return;
        }

        /** @var array $json */
        $json = json_decode($content, true);

        if (!$json) {
            $this->error('The composer.json is invalid.');
            return;
        }

        if (empty($json['require'])) {
            $this->error('No required packages.');
            return;
        }

        $packages = collect(array_keys($json['require']));
        $this->composerRequire($packages);

        if (!empty($json['require-dev'])) {
            $devPackages = collect(array_keys($json['require-dev']));
            $this->composerRequire($devPackages);
        }
    }

    public function composerRequire(Collection $packages): void
    {
        $command = [
            env('COMPOSER_BIN', 'composer'),
            'require',
            ...$packages->filter(
                function ($item) {
                    if ('php' === $item) {
                        return false;
                    }
                    return true;
                }
            ),
            '-W'
        ];

        $this->process($command);
    }
}
