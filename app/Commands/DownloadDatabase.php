<?php

namespace App\Commands;

use App\Actions\ConvertsSQLFileFromMariaDBToMysqlAction;
use App\Exceptions\FileTooSmallException;
use Illuminate\Support\Facades\Storage;
use App\Actions\ImportSQLFileToDatabaseAction;
use App\Actions\DownloadDatabaseAction;
use Illuminate\Support\Str;
use Symfony\Component\Process\Exception\ProcessFailedException;

class DownloadDatabase extends ProjectCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'download:database
                            {slug? : (optional) The short name for the project. By default it will use the current directory as the slug.}
                            {target? : (optional) Target directory to install to. By default it will use the slug of the project as the target directory.}
                            {--D|dry : Do a dry run (won\'t make any changes)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download database';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        [$project, $target] = $this->getProjectArguments();
        $instance = $project->getInstance('live');
        try {
            $filename = (new DownloadDatabaseAction($instance, $this))();
        } catch (\Exception $e) {
            $this->error(
                "An error occured when downloading database for ".
                "{$project->slug}:{$instance->slug}\n".
                $e->getMessage()
            );
            die(1);
        }

        if (!file_exists($filename) && !$this->option('dry')) {
            $this->error("Could not download $filename");
            die(1);
        }

        $this->info(($this->option('dry') ? 'Would import' : 'Importing')." {$filename} into {$target}");
        $import          = new ImportSQLFileToDatabaseAction($filename);
        $import->command = $this;
        try {
            $import($target);
        } catch (FileTooSmallException $e) {
            $this->error(
                "The downloaded database is empty"
            );
        } catch (ProcessFailedException $e) {
            if (!str_contains($e->getMessage(), 'PAGE_CHECKSUM')) {
                throw $e;
            }
            $uid      = uniqid();
            $tempPath = Str::finish(''.env('TB_TEMP_PATH', '/tmp'), '/');
            $tempFile = "{$tempPath}{$uid}.sql.gz";
            $this->info("Detected error importing MariaDB database to MySQL. Attempting automatic fix using temporary file {$tempFile}.");
            (new ConvertsSQLFileFromMariaDBToMysqlAction($filename))($tempFile);
            $import          = new ImportSQLFileToDatabaseAction($tempFile);
            $import->command = $this;
            if ('' === $import($target)) {
                $this->info("Success");
            }
            unlink($tempFile);
        }
        die(0);
    }
}
