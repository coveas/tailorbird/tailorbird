<?php


namespace App\Commands;

use LaravelZero\Framework\Commands\Command as ZeroCommand;
use Symfony\Component\Process\Process;

abstract class Command extends ZeroCommand
{
    protected function process(array $command):self
    {
        if ($this->option('dry') || $this->option('verbose')) {
            $this->info(($this->option('dry') ? 'Would run' : 'Running') .' the following command');
            $this->line(implode(' ', $command));
        }

        if (!$this->option('dry')) {
            $process = new Process($command);
            $timeout = floatval(env('COMMAND_TIMEOUT', 1800));
            $process->setTimeout($timeout);
            $process->setTty(true);
            $process->run();
        }
        return $this;
    }
}
