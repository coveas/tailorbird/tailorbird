<?php

namespace App\Commands;

use App\Actions\ConvertsSQLFileFromMariaDBToMysqlAction;
use App\Exceptions\FileTooSmallException;
use App\Factories\ProjectFactory;
use Illuminate\Support\Facades\Storage;
use App\Actions\ImportSQLFileToDatabaseAction;
use App\Actions\DownloadDatabaseAction;
use Illuminate\Support\Str;
use Symfony\Component\Process\Exception\ProcessFailedException;

class ServerCopyFiles extends ProjectCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'server:copy-files
                            {slug : The short name for the project. By default it will use the current directory as the slug.}
                            {instance: The instance to copy from}
                            {from: The from path (relative to the project root)}
                            {target_slug: The target project to copy to}
                            {target_instance: The target instance to copy to}
                            {to? : (optional) The target path (relative to the project root). Default is the same as the from path}
                            {--D|dry : Do a dry run (won\'t make any changes)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copy files between servers';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $slug    = $this->argument('slug');
        $factory = new ProjectFactory($this);
        try {
            $project = $factory->fromSlug($slug);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            die(1);
        }

        $instance = $project->getInstance($this->argument('instance'));
        $from = $this->argument('from');

        dd($project, $target, $instance, $from);
    }
}
