<?php

namespace App\Commands;

use App\Factories\ProjectFactory;

abstract class ProjectCommand extends Command
{
    public function getProjectArguments(bool $gitRequired = false): array
    {
        $slug    = $this->argument('slug');
        $target  = $this->argument('target');
        $factory = new ProjectFactory($this);

        if (! $slug) {
            $slug = getcwd();
            // Remove anything after . at the end of the slug
            $slug = preg_replace('/^.*\/(.*?)(\..*)?$/', '$1', "$slug");
            // Replace any non alphanumeric characters with underscore
            $slug = preg_replace('/[^a-z\d]/', '_', strtolower("$slug"));
        }

        if (! is_string($slug)) {
            $this->error('Invalid slug');
            die(1);
        }

        try {
            $project = $factory->fromSlug($slug);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            die(1);
        }
        if (!$target) {
            $target = $project->slug;
        }

        if (!$project->git_repo && $gitRequired) {
            $this->error("No git repo found for $slug");
            die(1);
        }
        return [$project, $target];
    }
}
