<?php

namespace App\Exceptions;

use Exception;
use JetBrains\PhpStorm\Pure;
use Throwable;

class FileTooSmallException extends Exception
{

    #[Pure] public function __construct(string $filename, int $code = 0, Throwable $previous = null)
    {
        parent::__construct(
            "File, $filename, is too small",
            $code,
            $previous
        );
    }
}
