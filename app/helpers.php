<?php

function tbconfig(string $key): string
{
    /** @var string $ssh_key */
    $ssh_key = env('SSH_KEY');
    if (!$ssh_key) {
        $ssh_key = getenv("HOME").'/.ssh/id_ed25519';
    }
    if (!file_exists($ssh_key)) {
        $ssh_key = getenv("HOME").'/.ssh/id_rsa';
    }
    return match ($key) {
        'api_provider' => ''.env('API_PROVIDER', 'https://tailorbird.dev/'),
        'api_key' => ''.env('API_KEY', ''),
        'ssh_key' => $ssh_key,
        'ssh_keyphrase' => ''.env('SSH_KEYPHRASE'),
        'default_user' => ''.env('DEFAULT_USER', 'forge'),
        default => ''
    };
}
